import DAO.*;
import DAO.entity.Orders;
import DAO.entity.Product;
import DAO.entity.Users;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import java.sql.SQLException;


public class RunnerApp {
    static final Logger logger = (Logger) LogManager.getLogger(RunnerApp.class);

    public static void main(String[] args) throws SQLException {


        logger.info("running app");
//        DAOchoose dbProc = new HibernateProcessor();
        DAOchoose dbProc = new JDBCProcessor();

        dbProc.addUser("Jack", "Paris");
        dbProc.deleteUser("Jack");
        dbProc.deleteUser("Ruslan");

        dbProc.addUser("Alexandr", "Ukraine");
        dbProc.updateUserName("Alexandr", "Ruslan");
        dbProc.deleteUser("Alexandr");
        dbProc.updateUserCountry("Ruslan", "UK");


        dbProc.addProduct("paper", "office white paper", (float) 18.98);
        dbProc.deleteProduct("paper");

        dbProc.addShoppingCart(new Orders(1, new Users(99,"Test_user","country")),
                new Product(9,"prouct_name","product_description", (float) 999.99),
                99);
        dbProc.deleteShoppingCart(255);

        dbProc.getAllOrders();
        dbProc.getAllProduct();
        dbProc.getAllShoppingCart();
        dbProc.getAllUser();


    }
}
