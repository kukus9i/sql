package DAO.entity;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.criterion.Order;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@DynamicUpdate
@Table(name = "Users")

public class Users implements Serializable {
    //можно не указывать Column name, если оно совпадает с названием столбца в таблице
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idUsers;

    @Column(name = "UserName")
    private String name;

    @Column(name = "Country")
    private String Country;

    public Users() {
    }

    public Users(String name, String country) {

        this.name = name;
        this.Country = country;
    }

    public Users(Integer id, String name, String country) {
        this.idUsers = id;
        this.name = name;
        this.Country = country;
    }


    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getUserName() {
        return name;
    }

    public void setUserName(String userName) {
        name = userName;
    }

    public int getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(int idUsers) {
        this.idUsers = idUsers;
    }

    @Override
    public String toString() {
        return
                idUsers +
                        " | " + name +
                        " | " + Country;
    }
}
