package DAO.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ShoppingCart")

public class ShoppingCart implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idCart")
    private int idCart;


    @Column(name = "Quantity")
    private int quantity;

    @ManyToOne
    @JoinColumn(name = "idProduct")
    private Product idProduct;

    @ManyToOne
    @JoinColumn(name = "idOrders")
    private Orders idOrders;


    public ShoppingCart() {

    }

    public ShoppingCart(Orders idOrders, Product idProduct, int quantity) {
        this.quantity = quantity;
        this.idProduct = idProduct;
        this.idOrders = idOrders;
    }

    public ShoppingCart(int idCart, int quantity, Product product, Orders orders) {
        this.idCart = idCart;
        this.quantity = quantity;
        this.idProduct = product;
        this.idOrders = orders;
    }

    public int getIdProduct() {
        return idProduct.getIdProduct();
    }

    public void setIdProduct(Product product) {
        this.idProduct = product;
    }

    public int getIdOrders() {
        return idOrders.getIdOrders();
    }

    public void setIdOrders(Orders orders) {
        this.idOrders = orders;
    }

    public int getIdCart() {
        return idCart;
    }

    public void setIdCart(int idCart) {
        this.idCart = idCart;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return idCart +
                " | " + idOrders.getIdOrders() +
                " | " + idProduct.getIdProduct() +
                " | " + quantity;
    }
}