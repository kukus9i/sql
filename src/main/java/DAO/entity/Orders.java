package DAO.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Orders")

public class Orders implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idOrders")
    private int idOrders;

    @ManyToOne
    @JoinColumn(name = "idUsers")
    private Users idUsers;

    public int getIdUsers() {
        return idUsers.getIdUsers();
    }

    public void setIdUsers(Users idUsers) {
        this.idUsers = idUsers;
    }


    public Orders() {

    }

    public Orders(Users users) {
        this.idUsers = users;
    }

    public Orders(int idOrders, Users idUsers) {
        this.idOrders = idOrders;
        this.idUsers = idUsers;
    }

    public int getIdOrders() {
        return idOrders;
    }

    public void setIdOrders(int idOrders) {
        this.idOrders = idOrders;
    }

    public String toString() {
        return
                idOrders +
                        " | " + idUsers.getIdUsers();
    }
}
