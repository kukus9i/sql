package DAO.model;

import DAO.HibernateSessionFactoryUtil;
import DAO.entity.Orders;
import DAO.entity.Users;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.Set;

public class OrdersDao {

    public Orders findById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Orders.class, id);
    }


    public void save(Orders orders) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(orders);
        tx1.commit();
        session.close();
    }

    public void update(Orders orders) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(orders);
        tx1.commit();
        session.close();
    }


    public void delete(Orders orders) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(orders);
        tx1.commit();
        session.close();
    }



    public List<Orders> findAll() {
        List<Orders> orders = (List<Orders>)  HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery("From Orders").list();
        return orders;
    }
}