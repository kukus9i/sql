package DAO.model;

import DAO.HibernateSessionFactoryUtil;
import DAO.entity.Orders;
import DAO.entity.Product;
import DAO.entity.ShoppingCart;
import DAO.entity.Users;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class ShoppingCartDao {

    public ShoppingCart findById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(ShoppingCart.class, id);
    }
    public List<ShoppingCart> findAll() {
        List<ShoppingCart> shoppingCartList = (List<ShoppingCart>)  HibernateSessionFactoryUtil.getSessionFactory()
                .openSession().createQuery("From ShoppingCart ").list();
        return shoppingCartList;
    }
    public void delete(ShoppingCart shoppingCart) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(shoppingCart);
        tx1.commit();
        session.close();
    }

    public void save(ShoppingCart shoppingCart) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(shoppingCart);
        tx1.commit();
        session.close();
    }

    public void update(ShoppingCart shoppingCart) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(shoppingCart);
        tx1.commit();
        session.close();
    }


}