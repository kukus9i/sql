package DAO;

import DAO.entity.Orders;
import DAO.entity.Product;
import DAO.entity.ShoppingCart;
import DAO.entity.Users;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JDBCProcessor implements DAOchoose {
    String url = "jdbc:mysql://localhost/shop";
    String bdUserName = "root";
    String password = "password";

    private static final Logger logger = (Logger) LogManager.getLogger(JDBCProcessor.class);

    @Override
    public void addUser(String username, String country) throws SQLException {
        Connection connection = null;
        Statement statement = null;
        connection = DriverManager.getConnection(url, bdUserName, password);
        statement = connection.createStatement();

        logger.info("Add user '" + username + "' successful.. from JDBC");
//        System.out.println("Executing statement... from JDBC");
//        System.out.println("Add user\t" + username + " successful");

        String sql = "INSERT INTO Users (UserName, Country) VALUES ( \"" + username + "\",\"" + country + "\")";
        statement.executeUpdate(sql);

        statement.close();
        connection.close();
    }

    @Override
    public void deleteUser(String username) throws SQLException {
        Connection connection = null;
        Statement statement = null;
        connection = DriverManager.getConnection(url, bdUserName, password);
        statement = connection.createStatement();

//        System.out.println("Executing statement... from JDBC");
//        System.out.println("User\t" + username + " - was deleted");
        logger.info("User\t" + username + " - was deleted from JDBC");
        String sql = "DELETE FROM Users" +
                " WHERE UserName='" + username + "';";
        statement.executeUpdate(sql);

        statement.close();
        connection.close();
    }

    @Override
    public void updateUserName(String username, String newUsername) throws SQLException {
        Connection connection = null;
        Statement statement = null;
        connection = DriverManager.getConnection(url, bdUserName, password);
        statement = connection.createStatement();

        System.out.println("Executing statement... from JDBC");

        String sql = "UPDATE Users SET UserName='" + newUsername +
                "' WHERE UserName='" + username + "'";
        statement.executeUpdate(sql);

        statement.close();
        connection.close();

    }

    @Override
    public void updateUserCountry(String username, String newCountry) throws SQLException {
        Connection connection = null;
        Statement statement = null;
        connection = DriverManager.getConnection(url, bdUserName, password);
        statement = connection.createStatement();

        System.out.println("Executing statement... from JDBC");

        String sql = "UPDATE Users SET Country='" + newCountry +
                "' WHERE userName='" + username + "'";
        statement.executeUpdate(sql);

        statement.close();
        connection.close();
    }

    @Override
    public List<Users> getAllUser() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        connection = DriverManager.getConnection(url, bdUserName, password);
        statement = connection.createStatement();

        List<Users> usersList = new ArrayList<>();
        ResultSet resultSet;

        logger.info("get all users from JDBC");
        String sql = "SELECT * FROM Users";
        resultSet = statement.executeQuery(sql);
        int i = 0;
        while (resultSet.next()) {
            usersList.add(i, new Users(resultSet.getInt("idUsers"),
                    resultSet.getString("UserName"),
                    resultSet.getString("Country")));

            logger.info(usersList.get(i));
            i++;
        }

        resultSet.close();
        statement.close();
        connection.close();
        return usersList;
    }


    @Override
    public List<Orders> getAllOrders() throws SQLException {
        List<Orders> ordersList = new ArrayList<>();
//        List<Orders> ordersList = (List<Orders>) new Orders();

        Connection connection = null;
        Statement statement, statement1;
        connection = DriverManager.getConnection(url, bdUserName, password);
        ResultSet resultSet, resultSet1 = null;
        statement = connection.createStatement();
        statement1 = connection.createStatement();

        logger.info("get all Orders from JDBC");

        String sql = "SELECT * FROM Orders ORDER BY idOrders";
        resultSet = statement.executeQuery(sql);
        int i = 0;
        while (resultSet.next()) {
            sql = "SELECT * FROM  users WHERE idUsers=" + resultSet.getInt("idUsers");
            resultSet1 = statement1.executeQuery(sql);
            if (resultSet1.next()) {
                ordersList.add(i, new Orders(
                        resultSet.getInt("idOrders"), new Users(
                        resultSet1.getInt("idUsers"),
                        resultSet1.getString("userName"),
                        resultSet1.getString("country"))));
            }
            logger.info(ordersList.get(i));
            i++;
        }
        resultSet.close();
        resultSet1.close();
        statement.close();
        statement1.close();
        connection.close();
        return ordersList;

    }

    @Override
    public void addOrders(Users users) throws SQLException {

    }

    @Override
    public void deleteOrders(int idOrders) throws SQLException {

    }

    @Override
    public void updateOrders() throws SQLException {

    }


    @Override
    public void addProduct(String productName, String description, float price) throws SQLException {
        Connection connection = null;
        Statement statement = null;
        connection = DriverManager.getConnection(url, bdUserName, password);
        statement = connection.createStatement();

        logger.info("add product '" + productName + "' successful.. from JDBC");
        String sql = "INSERT INTO Product (ProductName, Description, Price) VALUES ( \"" +
                productName + "\",\"" + description + "\",\"" + price + "\")";
        logger.debug(sql);
        statement.executeUpdate(sql);

        statement.close();
        connection.close();
    }

    @Override
    public void deleteProduct(String productName) throws SQLException {
        Connection connection = null;
        Statement statement = null;
        connection = DriverManager.getConnection(url, bdUserName, password);
        statement = connection.createStatement();

        logger.info("delete product '" + productName + "' successful.. from JDBC");
        String sql = "DELETE FROM Product" +
                " WHERE ProductName='" + productName + "'";
        statement.executeUpdate(sql);

        statement.close();
        connection.close();
    }


    @Override
    public List<Product> getAllProduct() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        connection = DriverManager.getConnection(url, bdUserName, password);
        statement = connection.createStatement();
        ResultSet resultSet;

        List<Product> productList = new ArrayList<>();

        logger.info("get all product... from JDBC");
        String sql = "SELECT * FROM Product  ORDER BY idProduct";
        resultSet = statement.executeQuery(sql);
        int i = 0;
        while (resultSet.next()) {
            productList.add(i, new Product(
                    resultSet.getInt("idProduct"),
                    resultSet.getString("ProductName"),
                    resultSet.getString("Description"),
                    resultSet.getFloat("Price")));

            logger.info(productList.get(i));
            i++;
        }
        resultSet.close();
        statement.close();
        connection.close();
        return productList;
    }

    @Override
    public List<ShoppingCart> getAllShoppingCart() throws SQLException {
        Connection connection = null;
        connection = DriverManager.getConnection(url, bdUserName, password);

        ResultSet resultSet = null,
                ordersResult = null,
                productResult = null,
                userResult = null;
        Statement statement = connection.createStatement(),
                orderStatement = connection.createStatement(),
                productStatement = connection.createStatement(),
                userStatement = connection.createStatement();
        List<ShoppingCart> shoppingCartList = new ArrayList<>();

        logger.info("get all shoppingcart... from JDBC");
        String sql = "SELECT * FROM ShoppingCart ORDER BY idCart";

        resultSet = statement.executeQuery(sql);

        for (int i = 0; resultSet.next(); i++) {

            sql = "SELECT * FROM  Orders WHERE idOrders=" + resultSet.getInt("idOrders");
            ordersResult = orderStatement.executeQuery(sql);
            sql = "SELECT * FROM  Product WHERE idProduct=" + resultSet.getInt("idProduct");
            productResult = productStatement.executeQuery(sql);

            if (ordersResult.next() && productResult.next()) {
                sql = "SELECT * FROM  users WHERE idUsers=" + ordersResult.getInt("idUsers");
                userResult = userStatement.executeQuery(sql);
                if (userResult.next()) {
                    shoppingCartList.add(i, new ShoppingCart(resultSet.getInt("idCart"),
                            resultSet.getInt("Quantity"),
                            new Product(
                                    productResult.getInt("idProduct"),
                                    productResult.getString("ProductName"),
                                    productResult.getString("Description"),
                                    productResult.getFloat("Price")),
                            new Orders(ordersResult.getInt("idOrders"), new Users(
                                    userResult.getInt("idUsers"),
                                    userResult.getString("userName"),
                                    userResult.getString("country")))));
                }
            }
            logger.info(shoppingCartList.get(i));
        }
//        resultSet = statement.executeQuery(sql);
//        while (resultSet.next()) {
//            int idCart = resultSet.getInt("idCart");
//            int idOrders = resultSet.getInt("idOrders");
//            int idProduct = resultSet.getInt("idProduct");
//            int quantity = resultSet.getInt("Quantity");
//
//            System.out.println(idCart +
//                    " | " + idOrders +
//                    " | " + idProduct +
//                    " | " + quantity);
//        }
        ordersResult.close();
        productResult.close();
        userResult.close();
        resultSet.close();
        statement.close();
        orderStatement.close();
        productStatement.close();
        userStatement.close();
        connection.close();
        return shoppingCartList;
    }

    @Override
    public void addShoppingCart(Orders orders, Product product, int quantity) throws SQLException {
        Connection connection = null;
        Statement statement = null;
        connection = DriverManager.getConnection(url, bdUserName, password);
        statement = connection.createStatement();

        logger.info("Add shoppingcart '" + orders + "' - order successful.. from JDBC");
//        System.out.println("Add shoppingcart " + idOrders + "... from JDBC");

        String sql = "INSERT INTO ShoppingCart (idOrders, idProduct, Quantity)" +
                "VALUES ( \"" + orders.getIdOrders() + "\",\"" + product.getIdProduct() + "\",\"" + quantity + "\" )";
        statement.executeUpdate(sql);
        logger.debug(sql);

        statement.close();
        connection.close();
    }

    @Override
    public void deleteShoppingCart(int idCart) throws SQLException {
        Connection connection = null;
        Statement statement = null;
        connection = DriverManager.getConnection(url, bdUserName, password);
        statement = connection.createStatement();

//        System.out.println("delete shoppingcart... from JDBC");
//        logger.info("delete shoppingcart id'" + idCart + "' successful.. from JDBC");
        String sql = "DELETE FROM Shoppingcart" +
                " WHERE idCart='" + idCart + "'";
        statement.executeUpdate(sql);
        logger.debug(sql);

        statement.close();
        connection.close();
    }
}
