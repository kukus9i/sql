package DAO;

import DAO.entity.Orders;
import DAO.entity.Product;
import DAO.entity.ShoppingCart;
import DAO.entity.Users;
import DAO.model.OrdersDao;
import DAO.model.ProductsDao;
import DAO.model.ShoppingCartDao;
import DAO.model.UserDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;


public class HibernateProcessor implements DAOchoose {

    private UserDao usersDao = new UserDao();
    private OrdersDao ordersDao = new OrdersDao();
    private ShoppingCartDao shoppingCartDao = new ShoppingCartDao();
    private ProductsDao productsDao = new ProductsDao();

    private static final Logger logger = (Logger) LogManager.getLogger(HibernateProcessor.class);

    public HibernateProcessor() {
    }


    @Override
    public List<Users> getAllUser() {
        logger.info("get all users from hibernate");
//        System.out.println("from Hibernate");
        List<Users> usersList = usersDao.findAll();
        for (Users i : usersList
        ) {
//            System.out.println(i.getUserName() + "\t|\t" + i.getCountry());
            logger.info(i);
        }
        return usersList;
    }


    @Override
    public void addUser(String username, String country) {
        logger.info("add new user '" + username + "' from hibernate");
        Users user = new Users(username, country);
        usersDao.save(user);
//        usersDao.update(user);

    }

    @Override
    public void deleteUser(String username) {
        List<Users> usersList = usersDao.findAll();

        for (Users i : usersList
        ) {
            if (i.getUserName().equals(username)) {
//                System.out.println("user\t" + username + " - was deleted");
                logger.info("delete user '" + username + "' from hibernate");
                usersDao.delete(i);

            }
        }
    }

    @Override
    public void updateUserName(String username, String newUsername) throws SQLException {
        List<Users> usersList = usersDao.findAll();
        Users users;
        for (Users i : usersList
        ) {
            if (i.getUserName().equals(username)) {
                logger.info("update user '" + username + "' to '" + newUsername + "' from hibernate");
                i.setUserName(newUsername);
                usersDao.update(i);
            }
        }
    }

    @Override
    public void updateUserCountry(String username, String newCountry) throws SQLException {
        List<Users> usersList = usersDao.findAll();
        logger.info("update Contry from '" + username + "' from hibernate");
        for (Users i : usersList
        ) {
            if (i.getUserName().equals(username)) {
                i.setCountry(newCountry);
                usersDao.update(i);
            }
        }
    }


    @Override
    public List<Orders> getAllOrders() throws SQLException {

        logger.info("get all orders from hibernate");
        List<Orders> ordersList = ordersDao.findAll();
        for (Orders i : ordersList
        ) {
            logger.info(i.getIdOrders() + "\t|\t" + i.getIdUsers());
        }
        return ordersList;
    }

    @Override
    public void addOrders(Users users) throws SQLException {
        Orders orders = new Orders(users);
        ordersDao.save(orders);
        ordersDao.update(orders);

    }

    @Override
    public void deleteOrders(int idOrders) throws SQLException {
        Orders orders = ordersDao.findById(idOrders);
        ordersDao.delete(orders);


    }

    @Override
    public void updateOrders() throws SQLException {

    }


    @Override
    public void addProduct(String productName, String description, float price) throws SQLException {
        logger.info("add new product '" + productName + "' from hibernate");
        Product product = new Product(productName, description, price);
        productsDao.save(product);
        productsDao.update(product);
    }

    @Override
    public void deleteProduct(String productName) throws SQLException {
        List<Product> productList = productsDao.findAll();

        for (Product i : productList
        ) {
            if (i.getProductName().equals(productName)) {
                logger.info("delete product '" + productName + "' from hibernate");
                productsDao.delete(i);

            }
        }
    }

    @Override
    public List<Product> getAllProduct() throws SQLException {
        logger.info("get all product from Hibernate");
        logger.info("id_product | name | description | price");
        List<Product> productList = productsDao.findAll();
        for (Product i : productList
        ) {
            System.out.println(i);
        }
        return productList;
    }

    @Override
    public void addShoppingCart(Orders orders, Product product, int quantity) throws SQLException {

//        logger.info("add new shopping cart from hibernate");
        ShoppingCart shoppingCart = new ShoppingCart(orders,product,quantity);
        shoppingCartDao.save(shoppingCart);
        shoppingCartDao.update(shoppingCart);
    }

    @Override
    public void deleteShoppingCart(int idCart) throws SQLException {
        if (shoppingCartDao.findById(idCart) != null) {
            logger.info("delete shopping cart by id '" + idCart + "' from hibernate");
            shoppingCartDao.delete(shoppingCartDao.findById(idCart));
        }


    }

    @Override
    public List<ShoppingCart> getAllShoppingCart() throws SQLException {
        logger.info("get all shopping cart from hibernate");
        System.out.println("id_cart | id_order | id_product | quantity");
        List<ShoppingCart> shoppingCartList = shoppingCartDao.findAll();
        for (ShoppingCart i : shoppingCartList
        ) {
            logger.info(i);
        }
        return shoppingCartList;
    }
}
