package DAO;

import DAO.entity.Orders;
import DAO.entity.Product;
import DAO.entity.ShoppingCart;
import DAO.entity.Users;

import java.sql.SQLException;
import java.util.List;

public interface DAOchoose {
    List<Users> getAllUser() throws SQLException;
    void addUser(String username, String country) throws SQLException;
    void deleteUser(String username)throws SQLException;
    void updateUserName(String username, String newUsername)throws SQLException;
    void updateUserCountry(String username, String newCountry)throws SQLException;

    List<Orders> getAllOrders() throws SQLException;
    void addOrders(Users users) throws SQLException;

    void deleteOrders(int idOrders) throws SQLException;
    void updateOrders() throws SQLException;


    void addProduct(String productName, String description, float price) throws SQLException;
    void deleteProduct(String productName)throws SQLException;
    List<Product> getAllProduct()throws SQLException;

//    void addShoppingCart(int idOrders, int idProduct, int quantity)throws SQLException;
    void addShoppingCart(Orders orders, Product product, int quantity)throws SQLException;
    void deleteShoppingCart(int idCart) throws SQLException;
    List<ShoppingCart> getAllShoppingCart()throws SQLException;
}
