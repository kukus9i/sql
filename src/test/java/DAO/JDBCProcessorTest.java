package DAO;

import DAO.entity.Orders;
import DAO.entity.Product;
import DAO.entity.ShoppingCart;
import DAO.entity.Users;
import com.mysql.cj.protocol.Resultset;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.jupiter.api.Assertions;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static org.junit.Assert.*;

public class JDBCProcessorTest {
    private static final Logger logger = (Logger) LogManager.getLogger(JDBCProcessorTest.class);
    private static Connection connection;
    private static Statement statement;
    private static ResultSet resultSet;
    private static String url = "jdbc:mysql://localhost/shop";
    private static String bdUserName = "root";
    private static String password = "password";

    private static String newCreatedUserName = "NEW_NAME";
    private static String newUpdatedUserName = "UPDATED_NAME";
    private static String newCreatedCountry = "NEW_COUNTRY";
    private static String newUpdatedCountry = "UPDATED_COUNTRY";

    private DAOchoose jdbcProcessor = new JDBCProcessor();


    @Before
    public void openSession() throws SQLException {
        connection = null;
        statement = null;
        resultSet = null;
        connection = DriverManager.getConnection(url, bdUserName, password);
        statement = connection.createStatement();
        logger.info("statement created");
        logger.info("connection created");

    }

    @After
    public void closeSession() throws SQLException {
        if (statement != null) {
            statement.close();
            logger.info("close statement");
        }
        if (connection != null) {
            connection.close();
            logger.info("close connection");
        }
        if (resultSet != null) {
            resultSet.close();
            logger.info("close result set");
        }
    }

    @Test
    public void addUser() throws SQLException {
        jdbcProcessor.addUser(newCreatedUserName, newCreatedCountry);
        String sql = "SELECT userName FROM  users WHERE userName='" + newCreatedUserName + "'";
        resultSet = statement.executeQuery(sql);
        while (resultSet.next()) {
            String userName = resultSet.getString("userName");
//            logger.info(userName + " equal " + newCreatedUserName);
            Assertions.assertEquals(newCreatedUserName, userName);
        }
        jdbcProcessor.deleteUser(newCreatedUserName);
    }

    @Test
    public void deleteUser() throws SQLException {
        jdbcProcessor.addUser(newCreatedUserName, newCreatedCountry);
        jdbcProcessor.deleteUser(newCreatedUserName);
        String sql = "SELECT userName FROM  users WHERE userName='" + newCreatedUserName + "'";
        resultSet = statement.executeQuery(sql);
        Assertions.assertFalse(resultSet.next());
        logger.info("Users has '" + newCreatedUserName + "' = " + resultSet.next());

    }

    @Test
    public void updateUserName() throws SQLException {
        jdbcProcessor.addUser(newCreatedUserName, newCreatedCountry);
        jdbcProcessor.updateUserName(newCreatedUserName, newUpdatedUserName);
        String sql = "SELECT userName FROM  users WHERE userName='" + newUpdatedUserName + "'";
        resultSet = statement.executeQuery(sql);
        while (resultSet.next()) {
            String userName = resultSet.getString("userName");
            logger.info(userName + " equal " + newCreatedUserName);
            Assertions.assertEquals(newUpdatedUserName, userName);
        }
        jdbcProcessor.deleteUser(newUpdatedUserName);

    }

    @Test
    public void updateUserCountry() throws SQLException {
        jdbcProcessor.addUser(newCreatedUserName, newCreatedCountry);
        jdbcProcessor.updateUserCountry(newCreatedUserName, newUpdatedCountry);
        String sql = "SELECT Country FROM  users WHERE Country='" + newUpdatedCountry + "'";
        resultSet = statement.executeQuery(sql);
        Assertions.assertTrue(resultSet.next());
        while (resultSet.next()) {
            String country = resultSet.getString("Country");
            logger.info("country equal " + country);
            Assertions.assertEquals(newUpdatedCountry, country);
        }
        jdbcProcessor.deleteUser(newCreatedUserName);

    }

    @Test
    public void getAllUser() throws SQLException {
        List<Users> users = jdbcProcessor.getAllUser();
        List<Users> usersFromDB = new ArrayList<>();
        String sql = "SELECT * FROM  users ";
        resultSet = statement.executeQuery(sql);
        int i = 0;
        while (resultSet.next()) {
            usersFromDB.add(i, new Users(resultSet.getInt("idUsers"),
                    resultSet.getString("UserName"),
                    resultSet.getString("Country")));
            i++;
        }
        for (i = 0; i < usersFromDB.size(); i++) {
//            logger.info("'" + String.valueOf(usersFromDB.get(i)) + "' equal '" + String.valueOf(users.get(i)) + "'");
            Assertions.assertTrue(String.valueOf(usersFromDB.get(i)).equals(String.valueOf(users.get(i))));
        }

    }

    @Test
    public void getAllOrders() throws SQLException {
        List<Orders> orders = jdbcProcessor.getAllOrders();
        List<Orders> ordersList = new ArrayList<>();

        ResultSet usersResult = null;
        Statement statement1 = connection.createStatement();

        String sql = "SELECT * FROM Orders ORDER BY idOrders";

        resultSet = statement.executeQuery(sql);
        int i = 0;
        while (resultSet.next()) {
            sql = "SELECT * FROM  users WHERE idUsers=" + resultSet.getInt("idUsers");
            usersResult = statement1.executeQuery(sql);
            if (usersResult.next()) {
                ordersList.add(i, new Orders(resultSet.getInt("idOrders"), new Users(
                        usersResult.getInt("idUsers"),
                        usersResult.getString("userName"),
                        usersResult.getString("country"))));
            }
            i++;
        }
        for (i = 0; i < ordersList.size(); i++) {
            logger.info("'" + String.valueOf(ordersList.get(i)) + "' equal '" + String.valueOf(orders.get(i)) + "'");
            Assertions.assertEquals(String.valueOf(ordersList.get(i)), String.valueOf(orders.get(i)));
        }
        usersResult.close();
        statement1.close();
    }

    @Test
    public void addOrders() {
    }

    @Test
    public void deleteOrders() {
    }

    @Test
    public void updateOrders() {
    }

    @Test
    public void addProduct() throws SQLException {

        String productName = "NEW_PRODUCT";
        String description = "NEW_DESCRIPTION";
        float price = (float) 9.99;
        String queryResult = null;
        String expectedResult = productName + description + price;

        jdbcProcessor.addProduct(productName, description, price);

        String sql = "SELECT * FROM product WHERE ProductName='" + productName + "'";
        resultSet = statement.executeQuery(sql);
        if (resultSet.next()) {
            queryResult = resultSet.getString("ProductName")
                    + resultSet.getString("Description")
                    + resultSet.getFloat("Price");

            logger.info("product from DB");
            logger.info(resultSet.getInt("idProduct") + " | "
                    + resultSet.getString("ProductName") + " | "
                    + resultSet.getString("Description") + " | "
                    + resultSet.getFloat("Price"));
        }

        Assertions.assertEquals(expectedResult, queryResult);
        jdbcProcessor.deleteProduct(productName);

    }

    @Test
    public void deleteProduct() throws SQLException {
        String productName = "NEW_PRODUCT";
        String description = "NEW_DESCRIPTION";
        float price = (float) 9.99;

        jdbcProcessor.addProduct(productName, description, price);
        jdbcProcessor.deleteProduct(productName);

        String sql = "SELECT * FROM product WHERE ProductName='" + productName + "'";
        resultSet = statement.executeQuery(sql);
        Assertions.assertFalse(resultSet.next());

    }

    @Test
    public void getAllProduct() throws SQLException {
        List<Product> product = jdbcProcessor.getAllProduct();
        List<Product> productList = new ArrayList<>();

        String sql = "SELECT * FROM Product ORDER BY idProduct";

        resultSet = statement.executeQuery(sql);
        int i = 0;
        while (resultSet.next()) {
            productList.add(i, new Product(
                    resultSet.getInt("idProduct"),
                    resultSet.getString("ProductName"),
                    resultSet.getString("Description"),
                    resultSet.getFloat("Price")));
//            logger.info(productList.get(i));
            i++;
        }
        for (i = 0; i < productList.size(); i++) {
//          logger.info("'" + String.valueOf(productList.get(i)) + "' equal '" + String.valueOf(product.get(i)) + "'");
            Assertions.assertTrue(String.valueOf(productList.get(i)).equals(String.valueOf(product.get(i))));
        }


    }

    @Test
    public void getAllShoppingCart() throws SQLException {
        List<ShoppingCart> shoppingCart = jdbcProcessor.getAllShoppingCart();
        List<ShoppingCart> shoppingCartList = new ArrayList<>();

        ResultSet ordersResult = null,
                productResult = null,
                userResult = null;
        Statement orderStatement = connection.createStatement(),
                productStatement = connection.createStatement(),
                userStatement = connection.createStatement();

        String sql = "SELECT * FROM ShoppingCart ORDER BY idCart";
        resultSet = statement.executeQuery(sql);
        for (int i = 0; resultSet.next(); i++) {
            sql = "SELECT * FROM  Orders WHERE idOrders=" + resultSet.getInt("idOrders");
            ordersResult = orderStatement.executeQuery(sql);
            sql = "SELECT * FROM  Product WHERE idProduct=" + resultSet.getInt("idProduct");
            productResult = productStatement.executeQuery(sql);

            if (ordersResult.next() && productResult.next()) {
                sql = "SELECT * FROM  users WHERE idUsers=" + ordersResult.getInt("idUsers");
                userResult = userStatement.executeQuery(sql);
                if (userResult.next()) {
                    shoppingCartList.add(i, new ShoppingCart(resultSet.getInt("idCart"),
                            resultSet.getInt("Quantity"),
                            new Product(
                                    productResult.getInt("idProduct"),
                                    productResult.getString("ProductName"),
                                    productResult.getString("Description"),
                                    productResult.getFloat("Price")),
                            new Orders(ordersResult.getInt("idOrders"), new Users(
                                    userResult.getInt("idUsers"),
                                    userResult.getString("userName"),
                                    userResult.getString("country")))));
                }
            }
            logger.info(shoppingCartList.get(i));
        }
        for (int i = 0; i < shoppingCartList.size(); i++) {
            logger.info("'" + String.valueOf(shoppingCartList.get(i))
                    + "' equal '" + String.valueOf(shoppingCart.get(i)) + "'");
            Assertions.assertTrue(String.valueOf(shoppingCartList.get(i)).equals(String.valueOf(shoppingCart.get(i))));
        }
        ordersResult.close();
        productResult.close();
        userResult.close();
        orderStatement.close();
        productStatement.close();
        userStatement.close();

    }

    @Test
    public void addShoppingCart() throws SQLException {
        String queryExcepted = null,
                queryResult = null;

        int idCart = 999,
                quantity = 999,
                idOrder = 9,
                idProduct = 9;
        Product product = new Product(idProduct, "NEW_PRODUCT", "NEW_DESCRIPTION", (float) 999.99);
        Orders order = new Orders(idOrder, new Users(999, "NEW_USER", "DEFAULT_CITY"));

        queryExcepted = String.valueOf(idOrder) +
                String.valueOf(idProduct) +
                String.valueOf(quantity);
        jdbcProcessor.addShoppingCart(order, product, quantity);

        String sql = "SELECT * FROM ShoppingCart WHERE idOrders=" + order.getIdOrders()
                + " AND idProduct=" + product.getIdProduct()
                + " AND Quantity=" + quantity;
        resultSet = statement.executeQuery(sql);
        logger.info(sql);
        while (resultSet.next()) {
            queryResult = String.valueOf(resultSet.getInt("idOrders")) +
                    String.valueOf(resultSet.getInt("idProduct")) +
                    String.valueOf(resultSet.getInt("Quantity"));
            jdbcProcessor.deleteShoppingCart(resultSet.getInt("idCart"));
//            logger.info(queryResult);
        }
        Assertions.assertEquals(queryExcepted, queryResult);


    }

    @Test
    public void deleteShoppingCart() throws SQLException {
        ResultSet deleteResult = null;
        Statement deleteStatement = connection.createStatement();

        int idCart = 999,
                quantity = 999,
                idOrder = 9,
                idProduct = 9;
        Product product = new Product(idProduct, "NEW_PRODUCT", "NEW_DESCRIPTION", (float) 999.99);
        Orders order = new Orders(idOrder, new Users(999, "NEW_USER", "DEFAULT_CITY"));
        jdbcProcessor.addShoppingCart(order, product, quantity);

        String sql = "SELECT * FROM ShoppingCart WHERE idOrders=" + order.getIdOrders()
                + " AND idProduct=" + product.getIdProduct()
                + " AND Quantity=" + quantity;

        resultSet = statement.executeQuery(sql);
        while (resultSet.next()) {
            jdbcProcessor.deleteShoppingCart(resultSet.getInt("idCart"));
            logger.info("delete shoppingcart id-" + resultSet.getInt("idCart"));
        }
        deleteResult = deleteStatement.executeQuery(sql);
        Assertions.assertFalse(deleteResult.next());
    }
}