package DAO;

import DAO.entity.Orders;
import DAO.entity.Product;
import DAO.entity.ShoppingCart;
import DAO.entity.Users;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.*;
import org.junit.jupiter.api.Assertions;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.From;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class HibernateProcessorTest {
    private static final Logger logger = (Logger) LogManager.getLogger(HibernateProcessorTest.class);
    private static SessionFactory sessionFactory;
    private static Session session;
    private static Integer id;
    private static String newCreatedUserName = "NEW_NAME";
    private static String newUpdatedUserName = "UPDATED_NAME";
    private static String newCreatedCountry = "NEW_COUNTRY";

    private DAOchoose hbnProc = new HibernateProcessor();

    @BeforeClass
    public static void setup() {
        sessionFactory = HibernateSessionFactoryUtil.getSessionFactory();
        logger.info("SessionFactory created");
    }

    @AfterClass
    public static void tearDown() {
        if (sessionFactory != null) sessionFactory.close();
        logger.info("SessionFactory destroyed");
    }


    @Before
    public void openSession() {
        session = sessionFactory.openSession();
        logger.info("session created");

    }

    @After
    public void closeSession() {
        if (session != null) session.close();
        logger.info("session closed");

    }

    @Test
    public void getAllUser() throws SQLException {

        List<Users> users = hbnProc.getAllUser();
        List<Users> sessionUsers = (List<Users>) sessionFactory.openSession().createQuery("From Users").list();

        for (int i = 0; i < sessionUsers.size(); i++) {
            logger.debug(String.valueOf(sessionUsers.get(i)) + " equal " + String.valueOf(users.get(i)));
            Assertions.assertTrue(String.valueOf(sessionUsers.get(i)).equals(String.valueOf(users.get(i))));

        }


    }

    @Test
    public void addUser() throws SQLException {
        hbnProc.addUser(newCreatedUserName, newCreatedCountry);

        Users users = null;
        List<Users> usersList = (List<Users>) sessionFactory.openSession().createQuery("FROM Users ").list();
        for (Users i : usersList
        ) {
            if (i.getUserName().equals(newCreatedUserName)) {
                users = i;
            }
        }

        Assertions.assertNotNull(users);
        logger.info(users);
        Assertions.assertEquals(newCreatedUserName, users.getUserName());
        logger.debug("user name - " + newCreatedUserName + " is equal - " + users.getUserName());
        hbnProc.deleteUser(newCreatedUserName);
    }

    @Test
    public void deleteUser() throws SQLException {
        hbnProc.addUser(newCreatedUserName, newCreatedCountry);
        Users users = null;
        List<Users> usersList = (List<Users>) sessionFactory.openSession().createQuery("FROM Users ").list();
        for (Users i : usersList
        ) {
            if (i.getUserName().equals(newCreatedUserName)) {
                users = i;

            }
        }
        hbnProc.deleteUser(newCreatedUserName);
        Users deleteUser = session.find(Users.class, users.getIdUsers());
        Assertions.assertNull(deleteUser);

    }

    @Test
    public void updateUserName() throws SQLException {

        hbnProc.addUser(newCreatedUserName, newCreatedCountry);
        hbnProc.updateUserName(newCreatedUserName, newUpdatedUserName);

        Users createdUsers = null, updatedUsers = null;
        List<Users> usersList = (List<Users>) sessionFactory.openSession().createQuery("FROM Users ").list();
        for (Users i : usersList
        ) {
            if (i.getUserName().equals(newCreatedUserName)) {
                createdUsers = i;

            }
            if (i.getUserName().equals(newUpdatedUserName)) {
                updatedUsers = i;

            }
        }
        Assertions.assertNull(createdUsers);
        Assertions.assertEquals(newUpdatedUserName, updatedUsers.getUserName());
        hbnProc.deleteUser(newUpdatedUserName);

    }

    @Test
    public void updateUserCountry() throws SQLException {

        String updatedCountry = "UPDATED_COUNTRY";
        hbnProc.addUser(newCreatedUserName, newCreatedCountry);
        hbnProc.updateUserCountry(newCreatedUserName, updatedCountry);

        Users updatedUsers = null, oldUsersCountry = null;
        List<Users> usersList = (List<Users>) sessionFactory.openSession().createQuery("FROM Users ").list();
        for (Users i : usersList
        ) {
            if (i.getUserName().equals(newCreatedUserName)) {
                updatedUsers = i;

            }
            if (i.getUserName().equals(newCreatedUserName) && i.getCountry().equals(newCreatedCountry)) {
                oldUsersCountry = i;

            }
        }
        Assertions.assertNull(oldUsersCountry);
        Assertions.assertEquals(updatedCountry, updatedUsers.getCountry());
        hbnProc.deleteUser(newCreatedUserName);

    }

    @Test
    public void getAllOrders() throws SQLException {
        List<Orders> orders = (List<Orders>) hbnProc.getAllOrders();
        List<Orders> sessionOrders = (List<Orders>) sessionFactory.openSession().createQuery("From Orders").list();

        for (int i = 0; i < sessionOrders.size(); i++) {
            logger.debug(String.valueOf(sessionOrders.get(i)) + " equal " + String.valueOf(orders.get(i)));
            Assertions.assertTrue(String.valueOf(sessionOrders.get(i)).equals(String.valueOf(orders.get(i))));

        }
    }

    @Test
    public void addOrders() throws SQLException {
        Users users = new Users(newCreatedUserName, newCreatedCountry);
        session.beginTransaction();
        id = (Integer) session.save(users);
        session.getTransaction().commit();
        users = null;
        users = session.find(Users.class, id);
        Assertions.assertEquals(newCreatedUserName, users.getUserName());

        hbnProc.addOrders(users);
        Orders orders = new Orders();
        List<Orders> ordersList = (List<Orders>) sessionFactory.openSession().createQuery("from Orders ").list();
        for (Orders i : ordersList) {
            if (i.getIdUsers() == id) {
                orders = i;
            }

        }
        Assertions.assertEquals(orders.getIdUsers(), id);

        hbnProc.deleteOrders(orders.getIdOrders());
        hbnProc.deleteUser(newCreatedUserName);

    }

    @Test
    public void deleteOrders() throws SQLException {
        Users users = new Users(newCreatedUserName, newCreatedCountry);
        session.beginTransaction();
        id = (Integer) session.save(users);
        session.getTransaction().commit();
        users = null;
        users = session.find(Users.class, id);
        Assertions.assertEquals(newCreatedUserName, users.getUserName());

        Orders orders = new Orders(users);
        session.beginTransaction();
        id = (Integer) session.save(orders);
        session.getTransaction().commit();
        logger.info(id);

        hbnProc.deleteOrders(id);

        hbnProc.deleteUser(newCreatedUserName);

        Orders deleteOrders = null;
        List<Orders> ordersList = (List<Orders>) sessionFactory.openSession().createQuery("FROM Orders ").list();
        for (Orders i : ordersList) {
            if (i.getIdOrders() == id) {
                deleteOrders = i;
            }
        }
        Assertions.assertNull(deleteOrders);
    }

    @Test
    public void updateOrders() {
    }

    @Test
    public void addProduct() throws SQLException {
        String productName = "NEW_PRODUCT";
        String description = "NEW_DESCRIPTION";
        float price = (float) 9.99;
        String queryResult = null;
        String expectedResult = productName + description + price;

        hbnProc.addProduct(productName, description, price);

        Product product = new Product();
        List<Product> productList = (List<Product>) sessionFactory.openSession().createQuery("FROM Product ").list();

        for (Product i : productList) {
            if (i.getProductName().equals(productName)) {
                product = i;
                queryResult = i.getProductName() + i.getDescription() + i.getPrice();
            }
        }
        Assertions.assertEquals(expectedResult, queryResult);
    }

    @Test
    public void deleteProduct() throws SQLException {
        String productName = "NEW_PRODUCT";
        String description = "NEW_DESCRIPTION";
        float price = (float) 9.99;

        Product product = new Product(productName, description, price);
        session.beginTransaction();
        id = (Integer) session.save(product);
        session.getTransaction().commit();

        hbnProc.deleteProduct(productName);
        product = null;
        List<Product> productList = (List<Product>) sessionFactory.openSession().createQuery("FROM Product ").list();
        for (Product i : productList) {
            if (i.getIdProduct() == id) {
                product = i;
            }
        }
        Assertions.assertNull(product);

        logger.info(product);


    }

    @Test
    public void getAllProduct() throws SQLException {
        List<Product> product = hbnProc.getAllProduct();
        List<Product> sessionProduct = (List<Product>) sessionFactory.openSession().createQuery("From Product").list();

        for (int i = 0; i < sessionProduct.size(); i++) {
            logger.info("'" + String.valueOf(sessionProduct.get(i)) + "' equal '" + String.valueOf(product.get(i)) + "'");
            Assertions.assertTrue(String.valueOf(sessionProduct.get(i)).equals(String.valueOf(product.get(i))));

        }

    }

    @Test
    public void addShoppingCart() throws SQLException {
        int idCart = 999,
                quantity = 999,
                idOrder = 9,
                idProduct = 9;
        Product product = new Product(idProduct, "NEW_PRODUCT", "NEW_DESCRIPTION", (float) 999.99);
        Orders order = new Orders(idOrder, new Users(999, "NEW_USER", "DEFAULT_CITY"));
        hbnProc.addShoppingCart(order, product, quantity);

        List<ShoppingCart> shoppingCartList = sessionFactory.openSession().createQuery("FROM ShoppingCart ").list();
        Assertions.assertNotNull(shoppingCartList);
        for (ShoppingCart i : shoppingCartList
        ) {
            if (i.getIdOrders() == idOrder && i.getIdProduct() == idProduct && i.getQuantity() == quantity) {
                logger.info("shoppingCart is exist " + i);
                Assertions.assertEquals(String.valueOf(idOrder + idProduct + quantity),
                        String.valueOf(i.getIdOrders() + i.getIdProduct() + i.getQuantity()));
                hbnProc.deleteShoppingCart(i.getIdCart());
            }

        }

    }

    @Test
    public void deleteShoppingCart() throws SQLException {
        int idCart = 999,
                quantity = 999,
                idOrder = 9,
                idProduct = 9;
        Product product = new Product(idProduct, "NEW_PRODUCT", "NEW_DESCRIPTION", (float) 999.99);
        Orders order = new Orders(idOrder, new Users(999, "NEW_USER", "DEFAULT_CITY"));
        hbnProc.addShoppingCart(order, product, quantity);
        List<ShoppingCart> shoppingCartList = sessionFactory.openSession().createQuery("FROM ShoppingCart ").list();
        ShoppingCart shoppingCart = null;
        for (ShoppingCart i : shoppingCartList
        ) {
            if ((i.getIdOrders() == idOrder) &&
                    (i.getIdProduct() == idProduct) &&
                    (i.getQuantity() == quantity)) {

                logger.info("shoppingCart is exist " + i);
                Assertions.assertEquals(String.valueOf(idOrder + idProduct + quantity),
                        String.valueOf(i.getIdOrders() + i.getIdProduct() + i.getQuantity()));
                id = i.getIdCart();

                logger.info(id);
                hbnProc.deleteShoppingCart(i.getIdCart());
            }
        }
        shoppingCart = session.find(ShoppingCart.class, id);
        Assertions.assertNull(shoppingCart);
        logger.info(shoppingCart);


    }

    @Test
    public void getAllShoppingCart() throws SQLException {
        List<ShoppingCart> shoppingCart = hbnProc.getAllShoppingCart();
        List<ShoppingCart> sessionShoppingCart = (List<ShoppingCart>) sessionFactory.openSession().
                createQuery("From ShoppingCart").list();

        for (int i = 0; i < sessionShoppingCart.size(); i++) {
            logger.debug(String.valueOf(sessionShoppingCart.get(i))
                    + " equal " + String.valueOf(shoppingCart.get(i)));

            Assertions.assertTrue(String.valueOf(sessionShoppingCart.get(i))
                    .equals(String.valueOf(shoppingCart.get(i))));
        }

    }


}