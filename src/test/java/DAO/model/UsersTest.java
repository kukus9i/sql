package DAO.model;

import DAO.entity.Users;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class UsersTest {
    public void testUsers() throws Exception {
        Configuration cfg = new Configuration().configure();

        // Создать SessionFactory
        SessionFactory sfactory = cfg.buildSessionFactory();

        // Создать Hibernate Session
        Session session = sfactory.openSession();

        try {
            // Начать транзакцию
            session.beginTransaction();

            // Создать объект сущности
            Users user = new Users();
            user.setUserName("Чжан Сан");
            user.setCountry("nikolaev");

            // Сохранить объект
            session.save(user);

            // Подтвердить транзакцию
            session.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }

}